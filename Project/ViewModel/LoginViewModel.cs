﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Model;
using Project.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Project.ViewModel
{
    public class LoginViewModel : BindableBase
    {
        private RestService restService;

        private string username;

        public string Username
        {
            get { return username; }
            set
            {
                SetProperty(ref username, value);
            }
        }

        public LoginViewModel()
        {
            restService = new RestService();
        }

        public string Login(string password)
        {
            int status = restService.Login(username, password);
            if (status == 400)
            {
                MessageBox.Show("Ошибка со статусом: " + status + "\nНеправильно набран логин или пароль");
                return "";
            }
            if (status != 200)
            {
                MessageBox.Show("Ошибка со статусом: " + status);
                return "";
            }
            else
                return restService.CheckUser();
        }
    }
}