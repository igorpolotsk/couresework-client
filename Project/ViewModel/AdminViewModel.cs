﻿using Prism.Commands;
using Prism.Mvvm;
using Project.ViewModel.AdminVM;
using System.Windows;

namespace Project.ViewModel
{
    public class AdminViewModel : BindableBase
    {
        private FacultyVM facultyVM;
        private SpecialtyVM specialtyVM;
        private DepartmentVM departmentVM;
        private GroupVM groupVM;
        private ThemeVM themeVM;
        private StudentVM studentVM;
        private TeacherVM teacherVM;

        public FacultyVM FacultyVM
        {
            get
            {
                if (facultyVM == null)
                {
                    facultyVM = new FacultyVM();
                }
                return facultyVM;
            }
        }

        public SpecialtyVM SpecialtyVM
        {
            get
            {
                if (specialtyVM == null)
                {
                    specialtyVM = new SpecialtyVM();
                }
                return specialtyVM;
            }
        }

        public DepartmentVM DepartmentVM
        {
            get
            {
                if (departmentVM == null)
                {
                    departmentVM = new DepartmentVM();
                }
                return departmentVM;
            }
        }

        public GroupVM GroupVM
        {
            get
            {
                if (groupVM == null)
                {
                    groupVM = new GroupVM();
                }
                return groupVM;
            }
        }

        public ThemeVM ThemeVM
        {
            get
            {
                if (themeVM == null)
                {
                    themeVM = new ThemeVM();
                }
                return themeVM;
            }
        }

        public StudentVM StudentVM
        {
            get
            {
                if (studentVM == null)
                {
                    studentVM = new StudentVM();
                }
                return studentVM;
            }
        }

        public TeacherVM TeacherVM
        {
            get
            {
                if (teacherVM == null)
                {
                    teacherVM = new TeacherVM();
                }
                return teacherVM;
            }
        }
    }
}