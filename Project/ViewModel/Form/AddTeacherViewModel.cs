﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddTeacherViewModel : BindableBase
    {
        private TeacherService teacherService;
        private DepartmentService departmentService;

        private TeacherService TeacherService
        {
            get
            {
                if (teacherService == null)
                {
                    teacherService = new TeacherService();
                }
                return teacherService;
            }
        }

        private DepartmentService DepartmentService
        {
            get
            {
                if (departmentService == null)
                {
                    departmentService = new DepartmentService();
                }
                return departmentService;
            }
        }

        private string name;
        private string surname;
        private string patronymic;
        private long idNumber;

        private Teacher teacher;
        private Department department;

        private ObservableCollection<Department> departments;

        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref name, value);
            }
        }
        
        public string Surname
        {
            get { return surname; }
            set
            {
                SetProperty(ref surname, value);
            }
        }

        public string Patronymic
        {
            get { return patronymic; }
            set
            {
                SetProperty(ref patronymic, value);
            }
        }

        public long IdNumber
        {
            get { return idNumber; }
            set
            {
                SetProperty(ref idNumber, value);
            }
        }

        public Teacher Teacher
        {
            get
            {
                if (teacher == null)
                {
                    teacher = new Teacher();
                }
                return teacher;
            }
            set
            {
                SetProperty(ref teacher, value);
            }
        }

        public Department Department
        {
            get { return department; }
            set
            {
                SetProperty(ref department, value);
            }
        }
        
        public ObservableCollection<Department> Departments
        {
            get 
            {
                if (departments == null)
                    departments = DepartmentService.FindAll();
                return departments;
            }
            set
            {
                SetProperty(ref departments, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveTeacher, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddTeacherViewModel()
        {
        }

        public AddTeacherViewModel(Teacher teacher)
        {
            Person person = new Person();
            person.Surname = teacher.Person.Surname;
            person.Name = teacher.Person.Name;
            person.Patronymic = teacher.Person.Patronymic;
            person.IdNumber = teacher.Person.IdNumber;

            Teacher = new Teacher
            {
                Id = teacher.Id,
                Person = teacher.Person,
                Departments = teacher.Departments
            };
        }

        private void SaveTeacher()
        {
            Person person = new Person();
            person.Name = name;
            person.Surname = surname;
            person.Patronymic = patronymic;
            person.IdNumber = idNumber;

            Teacher.Person = person;
            TeacherService.Save(Teacher);

            CurrentFrame.frame.Content = new AdminPage(6);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(6);
        }

        private bool CanSave()
        {
            return true;
        }
    }
}