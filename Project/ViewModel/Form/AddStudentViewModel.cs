﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddStudentViewModel : BindableBase
    {
        private StudentService studentService;
        private GroupService groupService;

        private StudentService StudentService
        {
            get
            {
                if (studentService == null)
                {
                    studentService = new StudentService();
                }
                return studentService;
            }
        }

        private GroupService GroupService
        {
            get
            {
                if (groupService == null)
                {
                    groupService = new GroupService();
                }
                return groupService;
            }
        }

        private string name;
        private string surname;
        private string patronymic;
        private long idNumber;

        private Student student;
        private Group selectedGroup;

        private ObservableCollection<Group> groups;

        public Student Student
        {
            get
            {
                if (student == null)
                {
                    student = new Student();
                }
                return student;
            }
            set
            {
                SetProperty(ref student, value);
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref name, value);
            }
        }
        public string Surname
        {
            get { return surname; }
            set
            {
                SetProperty(ref surname, value);
            }
        }
        public string Patronymic
        {
            get { return patronymic; }
            set
            {
                SetProperty(ref patronymic, value);
            }
        }
        public long IdNumber
        {
            get { return idNumber; }
            set
            {
                SetProperty(ref idNumber, value);
            }
        }

        public Group SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                SetProperty(ref selectedGroup, value);
            }
        }

        public ObservableCollection<Group> Groups
        {
            get
            {
                if (groups == null)
                {
                    groups = GroupService.FindAll();
                }
                return groups;
            }
            set
            {
                SetProperty(ref groups, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveStudent, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddStudentViewModel()
        {
        }

        public AddStudentViewModel(Student student)
        {
            Person person = new Person();
            person.Surname = student.Person.Surname;
            person.Name = student.Person.Name;
            person.Patronymic = student.Person.Patronymic;
            person.IdNumber = student.Person.IdNumber;

            Student = new Student
            {
                Id = student.Id,
                Person = person,
                AverageMark = student.AverageMark,
                Group = student.Group
            };
            SelectedGroup = student.Group;
        }

        private void SaveStudent()
        {
            Person person = new Person();
            person.Name = name;
            person.Surname = surname;
            person.Patronymic = patronymic;
            person.IdNumber = idNumber;
            
            Student.Person = person;
            Student.Group = SelectedGroup;
            StudentService.Save(Student);
            
            CurrentFrame.frame.Content = new AdminPage(5);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(5);
        }

        private bool CanSave()
        {
            if (SelectedGroup != null)
                return true;
            return false;
        }
    }
}