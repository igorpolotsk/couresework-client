﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddDepartmentViewModel : BindableBase
    {
        private DepartmentService departmentService;
        private FacultyService facultyService;

        private Department department;
        private Faculty selectedFaculty;
        
        private ObservableCollection<Faculty> faculties;

        private DepartmentService DepartmentService
        {
            get
            {
                if (departmentService == null)
                {
                    departmentService = new DepartmentService();
                }
                return departmentService;
            }
        }

        private FacultyService FacultyService
        {
            get
            {
                if (facultyService == null)
                {
                    facultyService = new FacultyService();
                }
                return facultyService;
            }
        }

        public Department Department
        {
            get 
            {
                if (department == null)
                {
                    department = new Department();
                }
                return department;
            }
            set
            {
                SetProperty(ref department, value);
            }
        }

        public Faculty SelectedFaculty
        {
            get
            {
                return selectedFaculty;
            }
            set
            {
                SetProperty(ref selectedFaculty, value);
            }
        }

        public ObservableCollection<Faculty> Faculties
        {
            get
            {
                if (faculties == null)
                {
                    faculties = FacultyService.FindAll();
                }
                return faculties;
            }
            set
            {
                SetProperty(ref faculties, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveDepartment, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddDepartmentViewModel()
        {
        }

        public AddDepartmentViewModel(Department department)
        {
            Department = new Department
            {
                Id = department.Id,
                Name = department.Name,
                Faculty = department.Faculty
                
            };
            SelectedFaculty = Department.Faculty;
        }

        private void SaveDepartment()
        {
            Department.Faculty = SelectedFaculty;
            DepartmentService.Save(Department);

            CurrentFrame.frame.Content = new AdminPage(2);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(2);
        }

        private bool CanSave()
        {
            if (Department != null && SelectedFaculty != null)
                return true;
            return false;
        }
    }
}