﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddSpecialtyViewModel : BindableBase
    {
        private SpecialtyService specialtyService;
        private FacultyService facultyService;

        private Specialty specialty;
        private Faculty selectedFaculty;

        private ObservableCollection<Faculty> faculties;

        private SpecialtyService SpecialtyService
        {
            get
            {
                if (specialtyService == null)
                {
                    specialtyService = new SpecialtyService();
                }
                return specialtyService;
            }
        }

        private FacultyService FacultyService
        {
            get
            {
                if (facultyService == null)
                {
                    facultyService = new FacultyService();
                }
                return facultyService;
            }
        }

        public Specialty Specialty
        {
            get
            {
                if (specialty == null)
                {
                    specialty = new Specialty();
                }
                return specialty;
            }
            set
            {
                SetProperty(ref specialty, value);
            }
        }

        public Faculty SelectedFaculty
        {
            get { return selectedFaculty; }
            set
            {
                SetProperty(ref selectedFaculty, value);
            }
        }

        public ObservableCollection<Faculty> Faculties
        {
            get
            {
                if (faculties == null)
                {
                    faculties = FacultyService.FindAll();
                }
                return faculties;
            }
            set
            {
                SetProperty(ref faculties, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveSpecialty, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddSpecialtyViewModel()
        {
        }

        public AddSpecialtyViewModel(Specialty specialty)
        {
            Specialty = new Specialty
            {
                Id = specialty.Id,
                Name = specialty.Name,
                Faculty = specialty.Faculty
            };
            SelectedFaculty = specialty.Faculty;
        }

        private void SaveSpecialty()
        {
            Specialty.Faculty = SelectedFaculty;
            SpecialtyService.Save(Specialty);
            
            CurrentFrame.frame.Content = new AdminPage(1);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(1);
        }

        private bool CanSave()
        {
            if (SelectedFaculty != null)
                return true;
            return false;
        }
    }
}