﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddGroupViewModel : BindableBase
    {
        private GroupService groupService;
        private SpecialtyService specialtyService;

        private Group group;
        private Specialty selectedSpecialty;

        private ObservableCollection<Specialty> specialties;

        private GroupService GroupService
        {
            get
            {
                if (groupService == null)
                {
                    groupService = new GroupService();
                }
                return groupService;
            }
        }

        private SpecialtyService SpecialtyService
        {
            get
            {
                if (specialtyService == null)
                {
                    specialtyService = new SpecialtyService();
                }
                return specialtyService;
            }
        }

        public Group Group
        {
            get
            {
                if(group == null)
                {
                    group = new Group();
                }
                return group;
            }
            set
            {
                SetProperty(ref group, value);
            }
        }
       
        public Specialty SelectedSpecialty
        {
            get { return selectedSpecialty; }
            set
            {
                SetProperty(ref selectedSpecialty, value);
            }
        }

        public ObservableCollection<Specialty> Specialties
        {
            get
            {
                if (specialties == null)
                {
                    specialties = SpecialtyService.FindAll();
                }
                return specialties;
            }
            set
            {
                SetProperty(ref specialties, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveGroup, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddGroupViewModel()
        {
        }

        public AddGroupViewModel(Group group)
        {
            Group = new Group
            {
                Id = group.Id,
                Number = group.Number,
                Course = group.Course,
                Specialty = group.Specialty
            };
            SelectedSpecialty = group.Specialty;
        }

        private void SaveGroup()
        {
            Group.Specialty = SelectedSpecialty;
            GroupService.Save(Group);
            
            CurrentFrame.frame.Content = new AdminPage(3);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(3);
        }

        private bool CanSave()
        {
            if (SelectedSpecialty != null)
                return true;
            return false;
        }
    }
}