﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddThemeViewModel : BindableBase
    {
        private ThemeService themeService;

        private ThemeService ThemeService
        {
            get
            {
                if (themeService == null)
                {
                    themeService = new ThemeService();
                }
                return themeService;
            }
        }

        private Theme theme;

        public Theme Theme
        {
            get 
            {
                if (theme == null)
                    theme = new Theme();
                return theme;
            }
            set
            {
                SetProperty(ref theme, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveTheme, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddThemeViewModel()
        {
        }

        public AddThemeViewModel(Theme theme)
        {
            Theme = new Theme
            {
                Id = theme.Id,
                Name = theme.Name
            };
        }

        private void SaveTheme()
        {
            ThemeService.Save(Theme);

            CurrentFrame.frame.Content = new AdminPage(4);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(4);
        }

        private bool CanSave()
        {
            return true;
        }
    }
}