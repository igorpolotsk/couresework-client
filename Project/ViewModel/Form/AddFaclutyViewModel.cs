﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View;
using System.Windows;
using System.Windows.Input;

namespace Project.ViewModel.Form
{
    public class AddFaclutyViewModel : BindableBase
    {
        private FacultyService facultyService;
        
        private Faculty faculty;

        private FacultyService FacultyService
        {
            get
            {
                if (facultyService == null)
                {
                    facultyService = new FacultyService();
                }
                return facultyService;
            }
        }

        public Faculty Faculty
        {
            get
            {
                if (faculty == null)
                {
                    faculty = new Faculty();
                }
                return faculty;
            }
            set
            {
                SetProperty(ref faculty, value);
            }
        }

        private ICommand saveCommand;
        private ICommand closeCommand;

        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(SaveFaculty, CanSave));
                }
                return saveCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (closeCommand == null)
                {
                    closeCommand = new DelegateCommand(CloseAction);
                }
                return closeCommand;
            }
        }

        public AddFaclutyViewModel()
        {
        }

        public AddFaclutyViewModel(Faculty faculty)
        {
            Faculty = new Faculty
            {
                Id = faculty.Id,
                Name = faculty.Name
            };
        }

        private void SaveFaculty()
        {
            FacultyService.Save(Faculty);

            CurrentFrame.frame.Content = new AdminPage(0);
        }

        private void CloseAction()
        {
            CurrentFrame.frame.Content = new AdminPage(0);
        }

        private bool CanSave()
        {
            if (Faculty != null)
            {
                return true;
            }
            return false;
        }
    }
}