﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class FacultyVM : BindableBase
    {
        private FacultyService facultyService;
        private Faculty selectedFaculty;
        private ObservableCollection<Faculty> faculties;

        private FacultyService FacultyService
        {
            get
            {
                if (facultyService == null)
                {
                    facultyService = new FacultyService();
                }
                return facultyService;
            }
        }

        public Faculty SelectedFaculty
        {
            get
            {
                return selectedFaculty;
            }
            set
            {
                SetProperty(ref selectedFaculty, value);
            }
        }

        public ObservableCollection<Faculty> Faculties
        {
            get
            {
                if (faculties == null)
                {
                    faculties = FacultyService.FindAll();
                }
                return faculties; 
            }
            private set
            {
                SetProperty(ref faculties, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddFaculty, IsNull));
                }
                return addCommand;
            }
        }
        
        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateFaculty, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteFaculty, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddFaculty()
        {
            CurrentFrame.frame.Content = new AddFacultyForm();
        }

        private void UpdateFaculty()
        {
            CurrentFrame.frame.Content = new AddFacultyForm(SelectedFaculty);
        }

        private void DeleteFaculty()
        {
            FacultyService.Delete(SelectedFaculty.Id);

            Faculties = FacultyService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedFaculty != null;
        }

        private bool IsNull()
        {
            return Faculties != null;
        }
    }
}