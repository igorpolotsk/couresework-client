﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class TeacherVM : BindableBase
    {
        private TeacherService teacherService;
        private Teacher selectedTeacher;
        private ObservableCollection<Teacher> teachers;

        private TeacherService TeacherService
        {
            get
            {
                if (teacherService == null)
                {
                    teacherService = new TeacherService();
                }
                return teacherService;
            }
        }

        public Teacher SelectedTeacher
        {
            get { return selectedTeacher; }
            set
            {
                SetProperty(ref selectedTeacher, value);
            }
        }

        public ObservableCollection<Teacher> Teachers
        {
            get
            {
                if (teachers == null)
                {
                    teachers = TeacherService.FindAll();
                }
                return teachers;
            }
            private set
            {
                SetProperty(ref teachers, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddTeacher, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateTeacher, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteTeacher, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddTeacher()
        {
            CurrentFrame.frame.Content = new AddTeacherForm();
        }

        private void UpdateTeacher()
        {
            //CurrentFrame.frame.Content = new AddTeacherForm(SelectedTeacher);
        }

        private void DeleteTeacher()
        {
            TeacherService.Delete(SelectedTeacher.Id);

            Teachers = TeacherService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedTeacher != null;
        }

        private bool IsNull()
        {
            return Teachers != null;
        }
    }
}