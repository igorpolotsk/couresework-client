﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class DepartmentVM : BindableBase
    {
        private DepartmentService departmentService;
        private Department selectedDepartment;
        private ObservableCollection<Department> departments;

        private DepartmentService DepartmentService
        {
            get
            {
                if (departmentService == null)
                {
                    departmentService = new DepartmentService();
                }
                return departmentService;
            }
        }

        public Department SelectedDepartment
        {
            get { return selectedDepartment; }
            set
            {
                SetProperty(ref selectedDepartment, value);
            }
        }

        public ObservableCollection<Department> Departments
        {
            get
            {
                if (departments == null)
                {
                    departments = DepartmentService.FindAll();
                }
                return departments;
            }
            private set
            {
                SetProperty(ref departments, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddDepartment, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateDepartment, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteDepartment, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddDepartment()
        {
            CurrentFrame.frame.Content = new AddDepartmentForm();
        }

        private void UpdateDepartment()
        {
            CurrentFrame.frame.Content = new AddDepartmentForm(SelectedDepartment);
        }

        private void DeleteDepartment()
        {
            DepartmentService.Delete(SelectedDepartment.Id);

            Departments = DepartmentService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedDepartment != null;
        }

        private bool IsNull()
        {
            return Departments != null;
        }
    }
}