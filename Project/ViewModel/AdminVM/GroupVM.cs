﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class GroupVM : BindableBase
    {
        private GroupService groupService;
        private Group selectedGroup;
        private ObservableCollection<Group> groups;

        private GroupService GroupService
        {
            get
            {
                if (groupService == null)
                {
                    groupService = new GroupService();
                }
                return groupService;
            }
        }

        public Group SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                SetProperty(ref selectedGroup, value);
            }
        }

        public ObservableCollection<Group> Groups
        {
            get
            {
                if (groups == null)
                {
                    groups = GroupService.FindAll();
                }
                return groups;
            }
            private set
            {
                SetProperty(ref groups, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddGroup, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateGroup, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteGroup, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddGroup()
        {
            CurrentFrame.frame.Content = new AddGroupForm();
        }

        private void UpdateGroup()
        {
            //CurrentFrame.frame.Content = new AddGroupForm(SelectedGroup);
        }

        private void DeleteGroup()
        {
            GroupService.Delete(SelectedGroup.Id);

            Groups = GroupService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedGroup != null;
        }

        private bool IsNull()
        {
            return Groups != null;
        }
    }
}