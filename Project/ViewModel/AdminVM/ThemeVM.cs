﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class ThemeVM : BindableBase
    {
        private ThemeService themeService;
        private Theme selectedTheme;
        private ObservableCollection<Theme> themes;

        private ThemeService ThemeService
        {
            get
            {
                if (themeService == null)
                {
                    themeService = new ThemeService();
                }
                return themeService;
            }
        }

        public Theme SelectedTheme
        {
            get { return selectedTheme; }
            set
            {
                SetProperty(ref selectedTheme, value);
            }
        }

        public ObservableCollection<Theme> Themes
        {
            get
            {
                if (themes == null)
                {
                    themes = ThemeService.FindAll();
                }
                return themes;
            }
            private set
            {
                SetProperty(ref themes, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddTheme, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateTheme, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteTheme, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddTheme()
        {
            CurrentFrame.frame.Content = new AddThemeForm();
        }

        private void UpdateTheme()
        {
            //CurrentFrame.frame.Content = new AddFacultyForm(SelectedFaculty);
        }

        private void DeleteTheme()
        {
            ThemeService.Delete(SelectedTheme.Id);

            Themes = ThemeService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedTheme != null;
        }

        private bool IsNull()
        {
            return Themes != null;
        }
    }
}