﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class StudentVM : BindableBase
    {
        private StudentService studentService;
        private Student selectedStudent;        
        private ObservableCollection<Student> students;

        private StudentService StudentService
        {
            get
            {
                if (studentService == null)
                {
                    studentService = new StudentService();
                }
                return studentService;
            }
        }

        public Student SelectedStudent
        {
            get { return selectedStudent; }
            set
            {
                SetProperty(ref selectedStudent, value);
            }
        }

        public ObservableCollection<Student> Students
        {
            get
            {
                if (students == null)
                {
                    students = StudentService.FindAll();
                }
                return students;
            }
            private set
            {
                SetProperty(ref students, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddStudent, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateStudent, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteStudent, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddStudent()
        {
            CurrentFrame.frame.Content = new AddStudentForm();
        }

        private void UpdateStudent()
        {
            //CurrentFrame.frame.Content = new AddStudentForm(SelectedStudent);
        }

        private void DeleteStudent()
        {
            StudentService.Delete(SelectedStudent.Id);

            Students = StudentService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedStudent != null;
        }

        private bool IsNull()
        {
            return Students != null;
        }
    }
}