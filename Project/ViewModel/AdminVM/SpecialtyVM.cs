﻿using Prism.Commands;
using Prism.Mvvm;
using Project.Commands;
using Project.Model;
using Project.Service;
using Project.View.Form;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Project.ViewModel.AdminVM
{
    public class SpecialtyVM : BindableBase
    {
        private SpecialtyService specialtyService;
        private Specialty selectedSpecialty;
        private ObservableCollection<Specialty> specialties;

        private SpecialtyService SpecialtyService
        {
            get
            {
                if (specialtyService == null)
                {
                    specialtyService = new SpecialtyService();
                }
                return specialtyService;
            }
        }

        public Specialty SelectedSpecialty
        {
            get { return selectedSpecialty; }
            set
            {
                SetProperty(ref selectedSpecialty, value);
            }
        }

        public ObservableCollection<Specialty> Specialties
        {
            get
            {
                if (specialties == null)
                {
                    specialties = SpecialtyService.FindAll();
                }
                return specialties;
            }
            private set
            {
                SetProperty(ref specialties, value);
            }
        }

        private ICommand addCommand;
        private ICommand updateCommand;
        private ICommand deleteCommand;

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(AddSpecialty, IsNull));
                }
                return addCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (updateCommand == null)
                {
                    updateCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(UpdateSpecialty, CanEdit));
                }
                return updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (deleteCommand == null)
                {
                    deleteCommand = new AutoCanExecuteCommandWrapper(new DelegateCommand(DeleteSpecialty, CanEdit));
                }
                return deleteCommand;
            }
        }

        private void AddSpecialty()
        {
            CurrentFrame.frame.Content = new AddSpecialtyForm();
        }

        private void UpdateSpecialty()
        {
            //CurrentFrame.frame.Content = new AddSpecialtyForm(SelectedSpecialty);
        }

        private void DeleteSpecialty()
        {
            SpecialtyService.Delete(SelectedSpecialty.Id);

            Specialties = SpecialtyService.FindAll();
        }

        private bool CanEdit()
        {
            return SelectedSpecialty != null;
        }

        private bool IsNull()
        {
            return Specialties != null;
        }
    }
}