﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class Theme : Entity
    {
        protected string name;

        [JsonProperty("name")]
        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref name, value);
            }
        }
    }
}