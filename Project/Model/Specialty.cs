﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class Specialty : Entity
    {
        protected string name;
        protected Faculty faculty;

        [JsonProperty("name")]
        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref name, value);
            }
        }

        [JsonProperty("faculty")]
        public Faculty Faculty
        {
            get { return faculty; }
            set
            {
                SetProperty(ref faculty, value);
            }
        }
    }
}