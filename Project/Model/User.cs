﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class User : Entity
    {
        private string username;
        private Role[] roles;

        [JsonProperty("username")]
        public string Username
        {
            get { return username; }
            set
            {
                SetProperty(ref username, value);
            }
        }

        [JsonProperty("roles")]
        public Role[] Roles
        {
            get { return roles; }
            set
            {
                SetProperty(ref roles, value);
            }
        }
    }
}