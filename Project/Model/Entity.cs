﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Entity : BindableBase
    {
        private long id;

        [JsonProperty("id")]
        public long Id
        {
            get { return id; }
            set
            {
                SetProperty(ref id, value);
            }
        }
    }
}