﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class Teacher : Entity
    {
        protected Person person;
        protected Department[] departments;

        [JsonProperty("person")]
        public Person Person
        {
            get { return person; }
            set
            {
                SetProperty(ref person, value);
            }
        }

        [JsonProperty("departments")]
        public Department[] Departments
        {
            get { return departments; }
            set
            {
                SetProperty(ref departments, value);
            }
        }
    }
}