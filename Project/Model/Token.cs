﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Token : BindableBase
    {
        private string access_token;
        private string token_type;
        private string expires_in;
        private string scope;
        private string jti;

        [JsonProperty("access_token")]
        public string Access_token
        {
            get { return access_token; }
            set
            {
                SetProperty(ref access_token, value);
            }
        }

        [JsonProperty("token_type")]
        public string Token_type
        {
            get { return token_type; }
            set
            {
                SetProperty(ref token_type, value);
            }
        }

        [JsonProperty("expires_in")]
        public string Expires_in
        {
            get { return expires_in; }
            set
            {
                SetProperty(ref expires_in, value);
            }
        }

        [JsonProperty("scope")]
        public string Scope
        {
            get { return scope; }
            set
            {
                SetProperty(ref scope, value);
            }
        }

        [JsonProperty("jti")]
        public string Jti
        {
            get { return jti; }
            set
            {
                SetProperty(ref jti, value);
            }
        }
    }
}