﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class Person : Entity
    {
        protected string name;
        protected string surname;
        protected string patronymic;
        protected long idNumber;

        [JsonProperty("name")]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
            }
        }

        [JsonProperty("surname")]
        public string Surname
        {
            get { return surname; }
            set
            {
                SetProperty(ref surname, value);
            }
        }

        [JsonProperty("patronymic")]
        public string Patronymic
        {
            get { return patronymic; }
            set
            {
                SetProperty(ref patronymic, value);
            }
        }

        [JsonIgnore]
        public string FullName
        {
            get { return name + " " + surname + " " + patronymic; }
            set
            {
                string fullname = name + " " + surname + " " + patronymic;
                SetProperty(ref fullname, value);
            }
        }

        [JsonProperty("idNumber")]
        public long IdNumber
        {
            get { return idNumber; }
            set
            {
                SetProperty(ref idNumber, value);
            }
        }
    }
}