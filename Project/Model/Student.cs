﻿
using Newtonsoft.Json;

namespace Project.Model
{
    public class Student : Entity
    {
        protected Person person;
        protected float averageMark;
        protected Group group;

        [JsonProperty("person")]
        public Person Person
        {
            get { return person; }
            set
            {
                SetProperty(ref person, value);
            }
        }

        [JsonProperty("averageMark")]
        public float AverageMark
        {
            get { return averageMark; }
            set
            {
                SetProperty(ref averageMark, value);
            }
        }

        [JsonProperty("group")]
        public Group Group
        {
            get { return group; }
            set
            {
                SetProperty(ref group, value);
            }
        }
    }
}