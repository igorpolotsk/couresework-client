﻿using Newtonsoft.Json;

namespace Project.Model
{
    public class Group : Entity
    {
        protected int number;
        protected int course;
        protected Specialty specialty;

        [JsonProperty("number")]
        public int Number
        {
            get { return number; }
            set
            {
                SetProperty(ref number, value);
            }
        }

        [JsonProperty("course")]
        public int Course
        {
            get { return course; }
            set
            {
                SetProperty(ref course, value);
            }
        }

        [JsonProperty("specialty")]
        public Specialty Specialty
        {
            get { return specialty; }
            set
            {
                SetProperty(ref specialty, value);
            }
        }
    }
}