﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Department : Entity
    {
        protected string name;
        protected Faculty faculty;

        private bool isSelected = false;

        [JsonProperty("name")]
        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref name, value);
            }
        }

        [JsonProperty("faculty")]
        public Faculty Faculty
        {
            get { return faculty; }
            set
            {
                SetProperty(ref faculty, value);
            }
        }

        [JsonIgnore]
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                SetProperty(ref isSelected, value);
            }
        }
    }
}