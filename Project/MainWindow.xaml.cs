﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Project.View;

namespace Project
{
    public static class CurrentFrame
    {
        public static Frame frame { get; set; }
    }

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            CurrentFrame.frame = new Frame();
            CurrentFrame.frame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            this.Content = CurrentFrame.frame;
            CurrentFrame.frame.Content = new LoginPage();
        }
    }
}