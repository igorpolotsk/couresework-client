﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class GroupService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Group> FindAll()
        {
            var request = new RestRequest("/groups", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Group>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Group>();
        }

        public Group GetItem(long id)
        {
            var request = new RestRequest("/groups/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Group data = JsonConvert.DeserializeObject<Group>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Group();
        }

        public void Save(Group group)
        {
            JObject jo = JObject.FromObject(group);
            jo["id"].Parent.Remove();
            string Object = JsonConvert.SerializeObject(jo);

            var request = new RestRequest("/groups", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/groups/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}