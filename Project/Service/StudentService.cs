﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class StudentService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Student> FindAll()
        {
            var request = new RestRequest("/students", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Student>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Student>();
        }

        public Student GetItem(long id)
        {
            var request = new RestRequest("/students/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Student data = JsonConvert.DeserializeObject<Student>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Student();
        }

        public void Save(Student student)
        {
            JObject jo = JObject.FromObject(student);
            jo["id"].Parent.Remove();
            jo["person"]["id"].Parent.Remove();
            jo["group"]["number"].Parent.Remove();
            jo["group"]["course"].Parent.Remove();
            jo["group"]["specialty"].Parent.Remove();
            
            string Object = JsonConvert.SerializeObject(jo);
            var request = new RestRequest("/students", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/students/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}