﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class DepartmentService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Department> FindAll()
        {
            var request = new RestRequest("/departments", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Department>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Department>();
        }

        public Department GetItem(long id)
        {
            var request = new RestRequest("/departments/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Department data = JsonConvert.DeserializeObject<Department>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Department();
        }

        public void Save(Department department)
        {
            JObject jo = JObject.FromObject(department);
            jo["id"].Parent.Remove();
            string Object = JsonConvert.SerializeObject(jo);

            var request = new RestRequest("/departments", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/departments/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}