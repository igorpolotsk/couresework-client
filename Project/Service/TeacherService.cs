﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class TeacherService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Teacher> FindAll()
        {
            var request = new RestRequest("/teachers", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Teacher>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Teacher>();
        }

        public Teacher GetItem(long id)
        {
            var request = new RestRequest("/teachers/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Teacher data = JsonConvert.DeserializeObject<Teacher>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Teacher();
        }

        public void Save(Teacher teacher)
        {
            JObject jo = JObject.FromObject(teacher);
            jo["id"].Parent.Remove();
            jo["person"]["id"].Parent.Remove();

            foreach (var doc in jo["departments"])
            {
                doc["faculty"].Parent.Remove();
            }

            string Object = JsonConvert.SerializeObject(jo);

            MessageBox.Show(Object);

            /*var request = new RestRequest("/teachers", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }*/
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/teachers/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}