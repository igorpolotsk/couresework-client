﻿using Newtonsoft.Json;
using Project.Model;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Windows;

namespace Project.Service
{
    public class RestService
    {
        private static Token Token { get; set; }

        private static RestClient Client { get; set; }

        protected static RestClient GetConnetion()
        {
            Client = new RestClient("http://localhost:8080");
            Client.AddDefaultHeader("Authorization", "Bearer " + Token.Access_token);
            return Client;
        }

        public int Login(string login, string password)
        {
            RestClient client = new RestClient("http://localhost:8080");
            client.Authenticator = new HttpBasicAuthenticator("testjwtclientid", "XY7kmzoNzl100");

            var request = new RestRequest("/oauth/token", RestSharp.Method.POST);
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", login);
            request.AddParameter("password", password);

            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful == false)
            {
                int status = (int)response.StatusCode;
                return status;
            }
            else
            {
                var result = response.Content;
                Token = JsonConvert.DeserializeObject<Token>(result);
                int status = (int)response.StatusCode;
                return status;
            }
        }

        public string CheckUser()
        {
            Client = GetConnetion();

            var request = new RestRequest("/users/me", RestSharp.Method.GET);

            IRestResponse response = Client.Execute(request);
            if (response.IsSuccessful == false)
            {
                MessageBox.Show("StatusCode: " + response.StatusCode);
                return null;
            }
            else
            {
                var result = response.Content;
                User user = JsonConvert.DeserializeObject<User>(result);

                Role role = (Role)user.Roles.GetValue(0);

                return role.Name;
            }
        }
    }
}