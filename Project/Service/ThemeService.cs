﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class ThemeService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Theme> FindAll()
        {
            var request = new RestRequest("/themes", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Theme>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Theme>();
        }

        public Theme GetItem(long id)
        {
            var request = new RestRequest("/themes/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Theme data = JsonConvert.DeserializeObject<Theme>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Theme();
        }

        public void Save(Theme theme)
        {
            JObject jo = JObject.FromObject(theme);
            jo["id"].Parent.Remove();
            string Object = JsonConvert.SerializeObject(jo);

            var request = new RestRequest("/themes", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/themes/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}