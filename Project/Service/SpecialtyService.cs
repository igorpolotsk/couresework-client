﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class SpecialtyService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Specialty> FindAll()
        {
            var request = new RestRequest("/specialties", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Specialty>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Specialty>();
        }

        public Specialty GetItem(long id)
        {
            var request = new RestRequest("/specialties/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Specialty data = JsonConvert.DeserializeObject<Specialty>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Specialty();
        }

        public void Save(Specialty specialty)
        {
            JObject jo = JObject.FromObject(specialty);
            jo["id"].Parent.Remove();
            string Object = JsonConvert.SerializeObject(jo);

            var request = new RestRequest("/specialties", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/specialties/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}
