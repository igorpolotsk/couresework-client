﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Project.Model;
using RestSharp;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;

namespace Project.Service
{
    public class FacultyService : RestService
    {
        private RestClient Client { get; set; } = GetConnetion();

        public ObservableCollection<Faculty> FindAll()
        {
            var request = new RestRequest("/faculties", RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                return JsonConvert.DeserializeObject<ObservableCollection<Faculty>>(result);
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new ObservableCollection<Faculty>();
        }

        public Faculty GetItem(long id)
        {
            var request = new RestRequest("/faculties/" + id, RestSharp.Method.GET);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode == 200)
            {
                var result = response.Content;
                Faculty data = JsonConvert.DeserializeObject<Faculty>(result);
                return data;
            }
            else
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
            return new Faculty();
        }

        public void Save(Faculty faculty)
        {
            JObject jo = JObject.FromObject(faculty);
            //jo["id"].Parent.Remove();
            string Object = JsonConvert.SerializeObject(jo);
            
            var request = new RestRequest("/faculties", RestSharp.Method.POST);
            request.AddJsonBody(Object);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 201)
            {
                MessageBox.Show(response.StatusCode.ToString() + response.Content);
            }
        }

        public void Delete(long id)
        {
            var request = new RestRequest("/faculties/" + id, RestSharp.Method.DELETE);
            IRestResponse response = Client.Execute(request);
            if ((int)response.StatusCode != 200)
            {
                MessageBox.Show(response.StatusCode.ToString());
            }
        }
    }
}