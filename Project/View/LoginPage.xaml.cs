﻿using Project.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Project.View
{
    /// <summary>
    /// Логика взаимодействия для LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private LoginViewModel loginViewModel;

        public LoginPage()
        {
            InitializeComponent();
            DataContext = loginViewModel = new LoginViewModel();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            string answer = loginViewModel.Login(password.Password);
            if (answer == "admin")
            {
                CurrentFrame.frame.Content = new AdminPage();
            }
            if (answer == "teacher")
            {
                
            }
            if (answer == "student")
            {
                
            }
        }
    }
}