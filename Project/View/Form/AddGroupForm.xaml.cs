﻿using Project.Model;
using Project.ViewModel.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project.View.Form
{
    /// <summary>
    /// Логика взаимодействия для AddGroupForm.xaml
    /// </summary>
    public partial class AddGroupForm : Page
    {
        public AddGroupForm()
        {
            InitializeComponent();
            DataContext = new AddGroupViewModel();
        }

        public AddGroupForm(object type)
        {
            InitializeComponent();

            if (type is Group)
            {
                Group group = type as Group;
                DataContext = new AddGroupViewModel(group);
            }
        }
    }
}