﻿using Project.Model;
using Project.ViewModel.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project.View.Form
{
    /// <summary>
    /// Логика взаимодействия для AddFacultyForm.xaml
    /// </summary>
    public partial class AddFacultyForm : Page
    {
        public AddFacultyForm()
        {
            InitializeComponent();
            DataContext = new AddFaclutyViewModel();
        }

        public AddFacultyForm(object type)
        {
            InitializeComponent();

            if (type is Faculty)
            {
                Faculty faculty = type as Faculty;
                DataContext = new AddFaclutyViewModel(faculty);
            }            
        }
    }
}