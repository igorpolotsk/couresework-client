﻿using Project.Model;
using Project.ViewModel.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project.View.Form
{
    /// <summary>
    /// Логика взаимодействия для AddSpecialtyForm.xaml
    /// </summary>
    public partial class AddSpecialtyForm : Page
    {
        public AddSpecialtyForm()
        {
            InitializeComponent();
            DataContext = new AddSpecialtyViewModel();
        }

        public AddSpecialtyForm(object type)
        {
            InitializeComponent();

            if (type is Specialty)
            {
                Specialty specialty = type as Specialty;
                DataContext = new AddSpecialtyViewModel(specialty);
            }
        }
    }
}
