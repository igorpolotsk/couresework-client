﻿using Project.Model;
using Project.ViewModel.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project.View.Form
{
    /// <summary>
    /// Логика взаимодействия для AddStudentForm.xaml
    /// </summary>
    public partial class AddStudentForm : Page
    {
        public AddStudentForm()
        {
            InitializeComponent();
            DataContext = new AddStudentViewModel();
        }

        public AddStudentForm(object type)
        {
            InitializeComponent();

            if (type is Student)
            {
                Student student = type as Student;
                DataContext = new AddStudentViewModel(student);
            }
        }
    }
}